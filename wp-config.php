<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'officecompany');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ePyggP8r>Z#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&YE>Gw`#S:u}wauDGS:o}$?cbs[X?85V$RBm05ho<yO}M5ssJW9OBl@yBd=);?)p');
define('SECURE_AUTH_KEY',  'uxyW:<2@q~a$Y,vA{-AWnhffj!XN}y5q>k@WrR%+P9WR$E<x90-L+CJf[l|.PQ|7');
define('LOGGED_IN_KEY',    '}r@AW.K&%&{jNQL3iB(YA.Qi&eXOdr[aaLD<!N1KhTWI/t2GRX]2}f2{]h6 j!nT');
define('NONCE_KEY',        '>}aDBhkxG?hVKi6~*@|i:CPhB|6fi:uU:Ul$s5(t70?%8bKd{hu Za/fjJdy|MEV');
define('AUTH_SALT',        'huqYGeqV1k`[oiaKIF@/#q3P.:r>ZB:}AU4 6owarFBehk/>NrJ]FO1^,E8<#l&g');
define('SECURE_AUTH_SALT', 'e(avBZNL.C%ZH]/U*$2FAEE8U:T rT9@tx?z,U,2Z&^5{>df3xZ,]n42P|^4919h');
define('LOGGED_IN_SALT',   'rb5Nu:)Ylvs.0EHm`;]j&@/`W4(%)lZ{|.D]um>7j&iV<`P2WdNh6OVA1|L^luQl');
define('NONCE_SALT',       '!VWv^=ONvk-+md8NelG-oH?GH+0xWL6A}rI8]Ky[~VPwW{.Tg<GPu$D]8/o vynF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'oc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
