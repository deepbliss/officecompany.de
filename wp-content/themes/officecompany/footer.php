<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<?php
         $logo = get_field('logo');
   
    ?>	

<footer class="container-fluid">
    <!--Wir stellen ein!-->
<section class="hiring container-fluid" style="background: url('<?php the_field('hiring_background_image', 'option') ?>') no-repeat center;">
    <div class="container  wow fadeInUp animated animated animated" data-wow-delay=".3s" style="visibility: visible;-webkit-animation-delay: .5s; -moz-animation-delay: .3s; 
        animation-delay: .3s;">  
            <h3><?php the_field('hiring_heading', 'option')?></h3>
            <div class="hiring-text"><?php the_field('hiring_text', 'option')?> </div>
            <a href="<?php the_field('hiring_link', 'option') ?>" target="_blank" class="detail-btn"><?php the_field('hiring_link_text', 'option')?></a>   
    </div>
</section>

<!--Footer Links-->
<section class="footer-links container-fluid" style="background: url('<?php the_field('footer_background_image', 'option') ?>') no-repeat center;">
    <div class="container">    
          <div class="footer-logo">
            <a href="<?php echo site_url(); ?>"><img src="<?php the_field('footer_logo','option');?>" alt="officecompany" title="officecompany"></a>
          </div> 
           <div class="footer-menu">
                           <?php wp_nav_menu( array('menu' => 'Footer Menu')); ?>       
           </div>     
          <div class="footer-support">
            <a href="<?php the_field('download_support_teamviewer','option'); ?>" target="_blank">Download Support TeamViewer MAC | WINDOWS</a>
          </div> 
          <div class="list-links">
            <ul>
                <li><a href="<?php the_field('facebook_link','option'); ?>" target="_blank" class="fb"></a></li>
                <li><a href="<?php the_field('instagram_link','option'); ?>" target="_blank" class="insta"></a></li>
                <li><a href="<?php the_field('twitter_link','option'); ?>" target="_blank" class="twitter"></a></li>
                <li><a href="<?php the_field('extra_link','option'); ?>" target="_blank" class="link"></a></li>
                <li><a href="<?php the_field('linkedin_link','option'); ?>" target="_blank" class="linkedin"></a></li>                  
            </ul>
          </div>
    </div>
</section>          
		
</footer>

</div>   <!--wrapper-->
<?php wp_footer(); ?>

<!--Scripts-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.all.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<!--parallax JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax.js/1.3.1/parallax.min.js"></script>
<!--slick-->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>


<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = '<?php echo site_url();?>/thank-you';
}, false );

/*parallax effect*/
$(document).ready(function() {
        $('.contact').parallax({
            imageSrc: '<?php the_field("contact_background_image"); ?>'
        });
});

</script>

</body>
</html>
