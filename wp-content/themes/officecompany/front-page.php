<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
    $main_banner_text_color = get_field('main_banner_text_color');
    if( !empty($main_banner_text_color) ){
        ?>
        <style>
            .main-banner h2{
                 color : <?php echo $main_banner_text_color; ?>;
             }
        </style>
        <?php
    }              
?>
<!--Banner-->
<section class="main-banner container-fluid" style="background: url('<?php the_field("main_banner_image"); ?>') no-repeat center;">
            <div class="container">
                    <div class="banner-box">
                        <h2><?php the_field("main_banner_text")?>  </h2>
                    </div>   
            </div>  
</section>

<!--Flip and service banner-->
<div id="flipbox" class="container-fluid"></div>
<section class="flip-box container-fluid wow fadeIn animated" style="visibility: visible;"> 
    <div class="container">
            <ul class="container-fluid topul">    
                  <?php if( have_rows('service_blocks') ):
                       while ( have_rows('service_blocks') ) : the_row(); ?>
                         <li class="flip-container wow fadeIn animated" data-wow-delay=".2s" style="visibility: visible; animation-delay: .2s;" 
                          ontouchstart="this.classList.toggle('hover');">
                              <div class="flipper container-fluid">
                                <div class="front  container-fluid">
                                  <div class="hservice-img container-fluid">
                                       <figure>
                                           <span>
                                               <img src="<?php the_sub_field('service_image'); ?>" alt="<?php the_sub_field('home_service_title'); ?>">
                                           </span>
                                       </figure>     
                                      
                                   </div>
                                   <div class="hservice-head  container-fluid"><p><?php the_sub_field('service_text'); ?></p></div>
                                   <div class="hservice-con container-fluid"><pre><?php the_sub_field('service_bottom_text'); ?></pre></div>
                                   
                              </div>               
                                <div class="back container-fluid">
                                      <div class="hservice-img container-fluid">
                                           <figure>
                                               <span>
                                                   <img src="<?php the_sub_field('flip_service_image'); ?>" alt="<?php the_sub_field('home_service_title'); ?>">
                                               </span>
                                           </figure>     
                                          
                                       </div>
                                       <div class="hservice-head container-fluid"><p><?php the_sub_field('flip_service_text'); ?></p></div>
                                       
                                       <div class="hservice-con container-fluid">
                                            <a class="hservicebtnh" href="<?php echo site_url();?>/#contact-form"><?php the_sub_field('flip_service_bottom_text'); ?></a>
                                       </div> 
                                      
                                </div>                       
                       </li>
                      <?php endwhile;
                  endif; ?>
            </ul> 
            
            <ul class="container-fluid servicebtnul">
                <div id="f1_container"  class="flip-container service-banner container-fluid wow fadeInUp animated" data-wow-delay=".2s"  style="visibility: visible;">
                    <div id="f1_card" class="shadow flipper container-fluid">
                      <div class="front face container-fluid">
                            <div class="flip-content">
                                <p><?php the_field('service_banner_text'); ?></p>
                                <div class="flip-link"><?php the_field('service_banner_bottom_text');?></div>
                            </div>
                      </div>
                      <div class="back face center   container-fluid">
                        <div class="flip-content">
                           <p><?php the_field('service_banner_text'); ?></p>
                           <div class="flip-link container-fluid">
                                <a href="<?php echo site_url();?>/#contact-form"><?php the_field('service_banner_bottom_text');?></a>
                           </div>
                        </div>
                      </div>
                    </div>
                </div>   
            </ul>    
    </div>
</section>

<!--work-->
<section class="work container-fluid">
    <div class="container">
        <h3>Wir arbeiten mit</h3>
         <?php do_shortcode('[work]' ); ?>                
    </div>
</section>

<!--company-->
<div id="company" class="container-fluid"></div>
<section class="company container-fluid" style="background: url('<?php the_field("company_background_image"); ?>') no-repeat center;">
    <div data-wow-delay=".5s" class="company-detail wow  wHighlight animated">
        <div class="company-detail-box">       
            <h3><?php the_field('company_title'); ?></h3>
            <div class="detail-text"><?php the_field('detail_about_company'); ?></div>
            <a href="<?php the_field('company_link') ?>" class="detail-btn"><?php the_field('company_link_text'); ?></a>   
        </div>                                    
    </div>
</section>

<!--Aufbau der office company Gruppe-->
<section class="group container-fluid">
    <div class="container">
        <h3>Aufbau der office company Gruppe</h3>           
        <div class="wow fadeInDown animated animated animated animated animated" data-wow-delay=".5s" style="visibility: visible;-webkit-animation-delay: .5s; -moz-animation-delay: .5s; animation-delay: .5s;">  
                <img src="<?php the_field('group_image'); ?>" alt="Aufbau der office company Gruppe"/>  
         </div>                                             
    </div>
</section>

<!--Highlighted Banner-->
<section class="bannerimg-text container-fluid">
   <div class="banner-img">
       <!-- <span><img src="<?php the_field('banner_image'); ?>" alt="Author"/></span> -->                                        
   </div> 
   <div class="banner-text">
      <div class="banner-innertext">
       <?php the_field('banner_text'); ?>
       </div>
    </div>  
</section>

<!--testimonial-->
<section class="testimonial container-fluid">
              <h3>Unsere Kunden</h3>
            <?php do_shortcode('[testimonial]' ); ?>                  
</section>

<!--contact-->
<div id="contact-form" class="container-fluid"></div>
<section class="contact container-fluid parallax">
    <div class="container">
         <div class="contact-left wow fadeInLeft animated" data-wow-delay=".35s" style="visibility: visible;animation-delay: 0.35s;">
            <h3><?php the_field('contact_left_heading')?></h3>
            <?php the_field('contact_detail')?>                       
         </div> 
         <div class="contact-right wow fadeInRight animated" data-wow-delay=".35s" style="visibility: visible;animation-delay: 0.35s;">
            <h3>Nachricht an uns</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmvod tempor incidid...</p>   
            <div class="contact-form">
               <?php echo do_shortcode( '[contact-form-7 id="130" title="Home Contact form"]')     ?>
            </div>                
         </div>                           
    </div>

</section>

<?php get_footer();
