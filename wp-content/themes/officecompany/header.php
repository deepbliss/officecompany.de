<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content = "telephone=no">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!--google font-->
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
<!--<link rel="icon" type="image/ico" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
-->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css">
<!--font-awesome-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<!--slick sliede-->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css">


<?php
    $logo = get_field('logo');
    $text_color = get_field('text_color');
    if( !empty($text_color) ){
        ?>
        <style>
             .header nav ul li a,header .head-call p, header .head-call a, .fontawe-color{
                 color : <?php echo $text_color; ?>;
             }
        </style>
        <?php
    } 
    $header_text_color_on_hover = get_field('header_text_color_on_hover');
    if( !empty($header_text_color_on_hover) ){
        ?>
        <style>
             header a:hover{
                 color : <?php echo $header_text_color_on_hover; ?> !important;
             }
        </style>
        <?php
    }
    if( empty($logo) ){
        $logo =   get_template_directory_uri().'/images/logo.png';               
    }
?>
<?php wp_head(); ?>
<style>
</style>
</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="container-fluid"> 
<header class="header container-fluid">
<!--Mobile btn-->
    <div  class="menu-btn">
        <a href="#menu">
            <i class="fas fa-bars fontawe-color"></i>           
        </a>     
                                
    </div>
<!-- logo      -->
    <div class="logo">
        <a href="<?php echo site_url(); ?>"  class="nonstickylogo"><img src="<?php echo $logo;?>" alt="officecompany" title="officecompany"></a>
        <a href="<?php echo site_url(); ?>"  class="stickylogo"><img src="<?php the_field('sticky_logo','option'); ?>" alt="officecompany" title="officecompany"></a>
    </div>

<!-- nav      -->
    <nav class="header-container desktop-menu">   
        <?php wp_nav_menu( array('menu' => 'Header Menu')); ?>       
    </nav>

<!-- mobile nav      -->
    <nav class="mobile-menu" id="menu">    
               <?php wp_nav_menu( array('menu' => 'Header Menu')); ?>       
    </nav>    
<!-- contact      -->
   <div class="mobile-call">
       <?php $variable = get_field('header_phone');?>
       <a href="tel:<?php the_field('header_phone','option'); ?>" class="mob-call"><i class="fas fa-phone fontawe-color"></i></a>
       <a href="mailto:<?php the_field('header_email','option');?>" class="mob-mail"><i class="fas fa-envelope fontawe-color"></i></a>
  </div>
                        
    <div class="head-call">    
         <?php $variable = get_field('header_phone');?>
         <p> <a href="tel:<?php the_field('header_phone','option'); ?>"><?php the_field('header_phone','option');?></a></p>
        <a href="mailto:<?php the_field('header_email','option');?>"><?php the_field('header_email','option');?></a>
    </div>
    
</header>

