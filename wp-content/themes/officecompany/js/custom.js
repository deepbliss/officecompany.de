/*Wow Script*/
new WOW().init();

/*Sticky header*/             

if($(window).width() <= 2000 && $(window).width() >= 1024)
{
$(window).scroll(function() {
if ($(this).scrollTop() > 145){ 
$('header.header').addClass("sticky");
}
else{
$('header.header').removeClass("sticky");
}    
});
}
if($(window).width() <= 1023 && $(window).width() >= 640)
{
$(window).scroll(function() {
if ($(this).scrollTop() > 75){ 
$('header.header').addClass("sticky");
}
else{
$('header.header').removeClass("sticky");
}    
});
}
if($(window).width() <= 639 && $(window).width() >= 414)
{
$(window).scroll(function() {
if ($(this).scrollTop() > 67){ 
$('header.header').addClass("sticky");
}
else{
$('header.header').removeClass("sticky");
}    
});
}
if($(window).width() <= 413 && $(window).width() >= 300)
{
$(window).scroll(function() {
if ($(this).scrollTop() > 42){ 
$('header.header').addClass("sticky");
}
else{
$('header.header').removeClass("sticky");
}    
});
}

$(document).ready(function(){  
/*Work Carousel*/
    /*    $('.work-carousel').owlCarousel({
        loop: true,
        items: 4,       
        autoplay: true,
        dots:false,
        smartSpeed:1000,
        autoplayTimeout:1000,
        rewindNav:true,
        autoHeight: true,
        margin:10,       
        nav: false,
        responsive: {         
        1024: {          
        items: 4,
        }, 
        767: {
        margin: 20,
        items: 4,
        }, 
        640: {
        margin: 20,
        items: 3,
        }, 
        567: {
        margin: 10,
        items: 3,
        }, 
        0: {
        items: 2,
        margin: 20,
        }
        }
    });*/
 
    
     /*testimonial-carousel Script*/
    
   $('.testimonial-carousel').owlCarousel({
        loop: true,
        items: 1,       
        autoplay: true,
        smartSpeed: 2500,
        dots:true,
        rewindNav:true,
        autoHeight: true,
        nav: false,
        responsive: {         
        1024: {        
        items: 1,
        },  
        640: {
        margin: 20,
        items: 1,
        }, 
        567: {
        margin: 10,
        items: 1,
        }, 
        0: {
        items: 1,
        margin: 0,
        }
        }
    });
        
});

<!--mobile menu-->

$(function() {
$('nav#menu').mmenu({
extensions : [ 'effect-slide-menu', 'pageshadow' ],
searchfield : false,
counters : false,
navbar : {
title : 'Menu'
},    
navbars : [
{
position : 'top',
<!--content : [ 'searchfield' ]-->               
}, {
position : 'top',
content : [
'prev',
'title',
'close'
]
}
]
});
});             
         
/*mobile menu open close on single page link*/
$(document).ready(function() {
  $(".mm-listview li").click(function(){
  $(this).parents('.mm-menu').removeClass('mm-opened');
  $('html').removeClass('mm-opened mm-blocking mm-background mm-effect-slide-menu mm-pageshadow mm-opening');   
}) 
   $(".menu-btn a").click(function(){
  $(this).parents('body').find('.mm-menu').addClass('mm-opened');
  $('html').addClass('mm-opened mm-blocking mm-background mm-effect-slide-menu mm-pageshadow mm-opening');
}) 
});


/*content comes from right when scrolled*/
var $animation_elements = $('.company-detail');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
      (element_top_position <= window_bottom_position)) {
      $element.addClass('fadeInRight');
    } else {
      $element.removeClass('fadeInRight');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');

jQuery(document).ready(function($) {
  $('.slick.marquee').slick({
    speed: 2000,
    autoplay: true,
    autoplaySpeed: 0,
    centerMode: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    infinite: true,
    fadeOut: true,
    initialSlide: 1,
    arrows: false,
    buttons: false
  });
});