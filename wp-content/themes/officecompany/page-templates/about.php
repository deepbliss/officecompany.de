<?php
/**
 * Template Name: About Page
 *
 **/
get_header(); ?>
<?php
    $main_banner_text_color = get_field('main_banner_text_color');
    if( !empty($main_banner_text_color) ){
        ?>
        <style>
            .main-banner h2{
                 color : <?php echo $main_banner_text_color; ?>;
             }
        </style>
        <?php
    }    
?>
<!--main banner-->
<section class="main-banner container-fluid" style="background: url('<?php the_field("main_banner_image"); ?>') no-repeat center;">
            <div class="container">
                    <div class="banner-box">
                        <h2><?php the_field("main_banner_text")?>  </h2>
                    </div>   
            </div> 
</section>

<!--Aufbau der office company Gruppe-->
<section class="group container-fluid">
    <div class="container">
        <h3>Aufbau der office company Gruppe</h3>           
        <img src="<?php the_field('group_image'); ?>" alt="Aufbau der office company Gruppe"/>                                        
    </div>
</section>


<!--company-->
<section class="company container-fluid" style="background: url('<?php the_field("company_background_image"); ?>') no-repeat center;" id="company">
    <div data-wow-delay=".5s" class="company-detail wow fadeInRight wHighlight animated" style="visibility: visible; animation-delay: 0.35s; animation-name: fadeInRight;">
        <div class="company-detail-box">       
            <h3><?php the_field('company_title'); ?></h3>
            <div class="detail-text"><?php the_field('detail_about_company'); ?></div>
         <!--   <a href="<?php the_field('company_link') ?>" class="detail-btn"><?php the_field('company_link_text'); ?></a>                                      -->
        </div>                                    
    </div>
</section>

<!--Das sind wir/our team-->
<section class="team container-fluid wow fadeIn animated" style="visibility: visible;"> 
     <div class="container">
        <div class="team-detail">     
            <h3>Das sind wir</h3>
            <p>Eine gute Wartung und eine regelmäßige Pflege sparen Kosten. Alle Office Company Service-Mitarbeiter werden regelmäßig geschult und sind auch für die 
            neuesten verfügbaren Systems zertifiziert.</p>
        </div>    
        <?php do_shortcode('[team]' ); ?>      
    </div>        
</section>
<?php get_footer(); ?>
