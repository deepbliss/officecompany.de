<?php
get_header(); ?>

	<?php
    $main_banner_text_color = get_field('main_banner_text_color');
    if( !empty($main_banner_text_color) ){
        ?>
        <style>
            .main-banner h2{
                 color : <?php echo $main_banner_text_color; ?>;
             }
        </style>
        <?php
    }    
?>
<!--main banner-->
<section class="main-banner container-fluid" style="background: url('<?php the_field("main_banner_image"); ?>') no-repeat center;">
            <div class="container">
                    <div class="banner-box">
                        <h2><?php the_field("main_banner_text")?>  </h2>
                    </div>   
            </div> 
</section>

<div class="main-content">
	<div class="container">
		<?php 
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); 
				the_content();
			} 
		} 
		?>
	</div>
</div>

<?php get_footer();
